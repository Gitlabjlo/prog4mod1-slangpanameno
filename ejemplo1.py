import sqlite3
from sqlite3 import Error

try:
   con = sqlite3.connect('mibasededatos.db')
   #con = sqlite3.connect(':memory:')
   print("Conexion exitosa")
except Error:
   print("Conexion fallida")

try:
   cur = con.cursor()
   cur.execute("CREATE TABLE IF NOT EXISTS heroe(id integer PRIMARY KEY, nombre text, comic text)")
   print("Creacion exitosa")
except Error:
   print("Error en creacion")

id = int(input("ID: "))
nombre = input("Nombre: ")
comic = input("Comic: ")

try:
   cur = con.cursor()
   #cur.execute("INSERT INTO heroe VALUES(1, 'Batman', 'DC')")
   cur.execute('''INSERT INTO heroe VALUES(?, ?, ? )''', (id, nombre, comic))
   con.commit()
   print("Insercion exitosa")
except Error:
   print("Error en insercion")

#try:
#   cur = con.cursor()
#   cur.execute('UPDATE heroe SET comic = "DC" WHERE nombre = "Superman"')
#   con.commit()
#   print("Actualizacion exitosa")
#except Error:
#   print("Error al actualizar")

try:
   cur = con.cursor()
   cur.execute('SELECT nombre FROM heroe WHERE comic = "DC"')
   heroes_dc = cur.fetchall()
   for heroe in heroes_dc:
      print(heroe)
except Error:
   print("Error al traer datos")

con.close()
